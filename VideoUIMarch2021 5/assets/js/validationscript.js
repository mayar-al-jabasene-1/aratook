$(document).ready(function() {

	//validation for add text only question:
	var inputfilled = 0;
	var checkedinput = 0;
	var editinputfilled = 0;
	var editcheckedinput = 0;

	var imginputfilled = 0;
	var imgcheckedinput = 0;
	var editimginputfilled = 0;
	var editimgcheckedinput = 0;

	var videoinputfilled = 0;
	var videocheckedinput = 0;
	var editvideoinputfilled = 0;
	var editvideocheckedinput = 0;

	var audioinputfilled = 0;
	var audiocheckedinput = 0;
	var editaudioinputfilled = 0;
	var editaudiocheckedinput = 0;

    $("#savetextQuestion").on('click', function(event) {
        event.preventDefault();
        for(var i = 0; i<4; i++) {
        	if ($('#response'+(i+1)).val() != '') {
        		inputfilled++;
        	}
        	if($('#checkresponse'+(i+1)).is(":checked")){
        		checkedinput++;
        	}
        }
        if($('#question').val() == '') {
        	document.getElementById('questiontextalert').style.display = "block";
    	} else {
    		document.getElementById('questiontextalert').style.display = "none";
    	}

	    if((inputfilled == checkedinput && inputfilled > 0) || inputfilled < checkedinput ) {
	    	document.getElementById('responsesequal').style.display = "block";
	    	if(inputfilled < 2) {
	    		document.getElementById('inputvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('inputvalidation').style.display = "none";
	    	}
	    	if(checkedinput < 1) {
	    		document.getElementById('checkboxvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('checkboxvalidation').style.display = "none";
	    	}
	    } else {
	    	document.getElementById('responsesequal').style.display = "none";
	    	if(inputfilled < 2) {
	    		document.getElementById('inputvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('inputvalidation').style.display = "none";
	    	}
	    	if(checkedinput < 1) {
	    		document.getElementById('checkboxvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('checkboxvalidation').style.display = "none";
	    	}
	    }
	    inputfilled = 0;
	    checkedinput = 0;
    });

    $("#edittextQuestion").on('click', function(event) {
        event.preventDefault();
        for(var i = 0; i<4; i++) {
        	if ($('#editresponse'+(i+1)).val() != '') {
        		editinputfilled++;
        	}
        	if($('#editcheckresponse'+(i+1)).is(":checked")){
        		editcheckedinput++;
        	}
        }
        if($('#editquestion').val() == '') {
        	document.getElementById('editquestiontextalert').style.display = "block";
    	} else {
    		document.getElementById('editquestiontextalert').style.display = "none";
    	}

	    if((editinputfilled == editcheckedinput && editinputfilled > 0) || editinputfilled < editcheckedinput ) {
	    	document.getElementById('editresponsesequal').style.display = "block";
	    	if(editinputfilled < 2) {
	    		document.getElementById('editinputvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('editinputvalidation').style.display = "none";
	    	}
	    	if(editcheckedinput < 1) {
	    		document.getElementById('editcheckboxvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('editcheckboxvalidation').style.display = "none";
	    	}
	    } else {
	    	document.getElementById('editresponsesequal').style.display = "none";
	    	if(editinputfilled < 2) {
	    		document.getElementById('editinputvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('editinputvalidation').style.display = "none";
	    	}
	    	if(editcheckedinput < 1) {
	    		document.getElementById('editcheckboxvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('editcheckboxvalidation').style.display = "none";
	    	}
	    }
	    editinputfilled = 0;
	    editcheckedinput = 0;
    });



    $("#saveimgQuestion").on('click', function(event) {
        event.preventDefault();
        for(var i = 0; i<4; i++) {
        	if ($('#imgresponse'+(i+1)).val() != '') {
        		imginputfilled++;
        	}
        	if($('#imgcheckresponse'+(i+1)).is(":checked")){
        		imgcheckedinput++;
        	}
        }

        if($('#imageUrl').val() == '' && $('#imgqstfile').val() == '') {
        	document.getElementById('imgfilealert').style.display = "block";
        } else {
        	document.getElementById('imgfilealert').style.display = "none";
        }
        if($('#imgqsttitle').val() == '') {
        	document.getElementById('imgquestiontextalert').style.display = "block";
    	} else {
    		document.getElementById('imgquestiontextalert').style.display = "none";
    	}

	    if((imginputfilled == imgcheckedinput && imginputfilled > 0) || imginputfilled < imgcheckedinput ) {
	    	document.getElementById('imgresponsesequal').style.display = "block";
	    	if(imginputfilled < 2) {
	    		document.getElementById('imginputvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('imginputvalidation').style.display = "none";
	    	}
	    	if(imgcheckedinput < 1) {
	    		document.getElementById('imgcheckboxvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('imgcheckboxvalidation').style.display = "none";
	    	}
	    } else {
	    	document.getElementById('imgresponsesequal').style.display = "none";
	    	if(imginputfilled < 2) {
	    		document.getElementById('imginputvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('imginputvalidation').style.display = "none";
	    	}
	    	if(imgcheckedinput < 1) {
	    		document.getElementById('imgcheckboxvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('imgcheckboxvalidation').style.display = "none";
	    	}
	    }

	    if(($('#imageUrl').val() != '' || $('#imgqstfile').val() != '') && ($('#imgqsttitle').val() != '') && imginputfilled >= 2 && imginputfilled > 0 && imgcheckedinput <=2 && imgcheckedinput>0) {
	    	document.getElementById("imgQuestionProgress").style.display = "block";
	    }
	    inputfilled = 0;
	    checkedinput = 0;
	    imginputfilled = 0;
	    imgcheckedinput = 0;
    });

    $("#editimgQuestion").on('click', function(event) {
        event.preventDefault();
        for(var i = 0; i<4; i++) {
        	if ($('#editimgresponse'+(i+1)).val() != '') {
        		editimginputfilled++;
        	}
        	if($('#editimgcheckresponse'+(i+1)).is(":checked")){
        		editimgcheckedinput++;
        	}
        }

        /*if($('#editimageUrl').val() == '' || $('#editimgquestion').val() == '') {
        	document.getElementById('editimgfilealert').style.display = "block";
        	alert($('#editimgquestion').val())
        } else {
        	document.getElementById('editimgfilealert').style.display = "none";
        }*/
        if($('#editimgqsttitle').val() == '') {
        	document.getElementById('editimgquestiontextalert').style.display = "block";
    	} else {
    		document.getElementById('editimgquestiontextalert').style.display = "none";
    	}

	    if((editimginputfilled == editimgcheckedinput && editimginputfilled > 0) || editimginputfilled < editimgcheckedinput ) {
	    	document.getElementById('editimgresponsesequal').style.display = "block";
	    	if(editimginputfilled < 2) {
	    		document.getElementById('editimginputvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('editimginputvalidation').style.display = "none";
	    	}
	    	if(editimgcheckedinput < 1) {
	    		document.getElementById('editimgcheckboxvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('editimgcheckboxvalidation').style.display = "none";
	    	}
	    } else {
	    	document.getElementById('editimgresponsesequal').style.display = "none";
	    	if(editimginputfilled < 2) {
	    		document.getElementById('editimginputvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('editimginputvalidation').style.display = "none";
	    	}
	    	if(editimgcheckedinput < 1) {
	    		document.getElementById('editimgcheckboxvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('editimgcheckboxvalidation').style.display = "none";
	    	}
	    }

	    if(($('#editimageUrl').val() != '' || $('#editimgquestion').val() != '') && ($('#editimgqsttitle').val() != '') && editimginputfilled >= 2 && editimginputfilled > 0 && editimgcheckedinput <=2 && editimgcheckedinput>0) {
	    	document.getElementById("editimgqstProgress").style.display = "block";
	    }
	    editinputfilled = 0;
	    editcheckedinput = 0;
	    editimginputfilled = 0;
	    editimgcheckedinput = 0;
    });

    $("#savevideoQuestion").on('click', function(event) {
        event.preventDefault();
        for(var i = 0; i<4; i++) {
        	if ($('#videoresponse'+(i+1)).val() != '') {
        		videoinputfilled++;
        	}
        	if($('#videocheckresponse'+(i+1)).is(":checked")){
        		videocheckedinput++;
        	}
        }

        if($('#videoUrl').val() == '' && $('#videoqstfile').val() == '') {
        	document.getElementById('videofilealert').style.display = "block";
        } else {
        	document.getElementById('videofilealert').style.display = "none";
        }

        if($('#videoqsttitle').val() == '') {
        	document.getElementById('videoquestiontextalert').style.display = "block";
    	} else {
    		document.getElementById('videoquestiontextalert').style.display = "none";
    	}

	    if((videoinputfilled == videocheckedinput && videoinputfilled > 0) || videoinputfilled < videocheckedinput ) {
	    	document.getElementById('videoresponsesequal').style.display = "block";
	    	if(videoinputfilled < 2) {
	    		document.getElementById('videoinputvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('videoinputvalidation').style.display = "none";
	    	}
	    	if(videocheckedinput < 1) {
	    		document.getElementById('videocheckboxvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('videocheckboxvalidation').style.display = "none";
	    	}
	    } else {
	    	document.getElementById('videoresponsesequal').style.display = "none";
	    	if(videoinputfilled < 2) {
	    		document.getElementById('videoinputvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('videoinputvalidation').style.display = "none";
	    	}
	    	if(videocheckedinput < 1) {
	    		document.getElementById('videocheckboxvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('videocheckboxvalidation').style.display = "none";
	    	}
	    }

	    if(($('#videoUrl').val() != '' || $('#videoqstfile').val() != '') && ($('#videoqsttitle').val() != '') && videoinputfilled >= 2 && videoinputfilled > 0 && videocheckedinput <=2 && videocheckedinput>0) {
	    	document.getElementById("editvideoElementProgress").style.display = "block";
	    }
	    videoinputfilled = 0;
	    videocheckedinput = 0;
    });

    $("#editvideoQuestion").on('click', function(event) {
        event.preventDefault();
        for(var i = 0; i<4; i++) {
        	if ($('#editvideoresponse'+(i+1)).val() != '') {
        		editvideoinputfilled++;
        	}
        	if($('#editvideocheckresponse'+(i+1)).is(":checked")){
        		editvideocheckedinput++;
        	}
        }

        /*if($('#editvideoUrl').val() == '' && $('#editvideoquestion').val() == '') {
        	document.getElementById('editvideofilealert').style.display = "block";
        } else {
        	document.getElementById('editvideofilealert').style.display = "none";
        }*/

        if($('#editvideoqsttitle').val() == '') {
        	document.getElementById('editvideoquestiontextalert').style.display = "block";
    	} else {
    		document.getElementById('editvideoquestiontextalert').style.display = "none";
    	}

	    if((editvideoinputfilled == editvideocheckedinput && editvideoinputfilled > 0) || editvideoinputfilled < editvideocheckedinput ) {
	    	document.getElementById('editvideoresponsesequal').style.display = "block";
	    	if(editvideoinputfilled < 2) {
	    		document.getElementById('editvideoinputvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('editvideoinputvalidation').style.display = "none";
	    	}
	    	if(editvideocheckedinput < 1) {
	    		document.getElementById('editvideocheckboxvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('editvideocheckboxvalidation').style.display = "none";
	    	}
	    } else {
	    	document.getElementById('editvideoresponsesequal').style.display = "none";
	    	if(editvideoinputfilled < 2) {
	    		document.getElementById('editvideoinputvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('editvideoinputvalidation').style.display = "none";
	    	}
	    	if(editvideocheckedinput < 1) {
	    		document.getElementById('editvideocheckboxvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('editvideocheckboxvalidation').style.display = "none";
	    	}
	    }

	    if(($('#editvideoUrl').val() != '' || $('#editvideoquestion').val() != '') && ($('#editvideoqsttitle').val() != '') && editvideoinputfilled >= 2 && editvideoinputfilled > 0 && editvideocheckedinput <=2 && editvideocheckedinput>0) {
	    	document.getElementById("editvideoqstProgress").style.display = "block";
	    }
	    editvideoinputfilled = 0;
	    editvideocheckedinput = 0;
    });

    $("#saveaudioQuestion").on('click', function(event) {
        event.preventDefault();
        for(var i = 0; i<4; i++) {
        	if ($('#audioresponse'+(i+1)).val() != '') {
        		audioinputfilled++;
        	}
        	if($('#audiocheckresponse'+(i+1)).is(":checked")){
        		audiocheckedinput++;
        	}
        }

        if($('#audioUrl').val() == '' && $('#audio-file-selector').val() == '') {
        	document.getElementById('audiofilealert').style.display = "block";
        } else {
        	document.getElementById('audiofilealert').style.display = "none";
        }

        if($('#audioqsttitle').val() == '') {
        	document.getElementById('audioquestiontextalert').style.display = "block";
    	} else {
    		document.getElementById('audioquestiontextalert').style.display = "none";
    	}

	    if((audioinputfilled == audiocheckedinput && audioinputfilled > 0) || audioinputfilled < audiocheckedinput ) {
	    	document.getElementById('audioresponsesequal').style.display = "block";
	    	if(audioinputfilled < 2) {
	    		document.getElementById('audioinputvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('audioinputvalidation').style.display = "none";
	    	}
	    	if(audiocheckedinput < 1) {
	    		document.getElementById('audiocheckboxvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('audiocheckboxvalidation').style.display = "none";
	    	}
	    } else {
	    	document.getElementById('audioresponsesequal').style.display = "none";
	    	if(audioinputfilled < 2) {
	    		document.getElementById('audioinputvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('audioinputvalidation').style.display = "none";
	    	}
	    	if(audiocheckedinput < 1) {
	    		document.getElementById('audiocheckboxvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('audiocheckboxvalidation').style.display = "none";
	    	}
	    }

	    if(($('#audioUrl').val() != '' || $('#audio-file-selector').val() != '') && ($('#audioqsttitle').val() != '') && audioinputfilled >= 2 && audioinputfilled > 0 && audiocheckedinput <=2 && audiocheckedinput>0) {
	    	document.getElementById("audioQuestionProgress").style.display = "block";
	    }

	    audioinputfilled = 0;
	    audiocheckedinput = 0;
    });

    $("#editaudioQuestion").on('click', function(event) {
        event.preventDefault();
        for(var i = 0; i<4; i++) {
        	if ($('#editaudioresponse'+(i+1)).val() != '') {
        		editaudioinputfilled++;
        	}
        	if($('#editaudiocheckresponse'+(i+1)).is(":checked")){
        		editaudiocheckedinput++;
        	}
        }

        /*if($('#editaudioUrl').val() == '' && $('#edit-audio-file-selector').val() == '') {
        	document.getElementById('editaudiofilealert').style.display = "block";
        } else {
        	document.getElementById('editaudiofilealert').style.display = "none";
        }*/

        if($('#editaudioqsttitle').val() == '') {
        	document.getElementById('editaudioquestiontextalert').style.display = "block";
    	} else {
    		document.getElementById('editaudioquestiontextalert').style.display = "none";
    	}

	    if((editaudioinputfilled == editaudiocheckedinput && editaudioinputfilled > 0) || editaudioinputfilled < editaudiocheckedinput ) {
	    	document.getElementById('editaudioresponsesequal').style.display = "block";
	    	if(editaudioinputfilled < 2) {
	    		document.getElementById('editaudioinputvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('editaudioinputvalidation').style.display = "none";
	    	}
	    	if(editaudiocheckedinput < 1) {
	    		document.getElementById('editaudiocheckboxvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('editaudiocheckboxvalidation').style.display = "none";
	    	}
	    } else {
	    	document.getElementById('editaudioresponsesequal').style.display = "none";
	    	if(editaudioinputfilled < 2) {
	    		document.getElementById('editaudioinputvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('editaudioinputvalidation').style.display = "none";
	    	}
	    	if(editaudiocheckedinput < 1) {
	    		document.getElementById('editaudiocheckboxvalidation').style.display = "block";
	    	} else {
	    		document.getElementById('editaudiocheckboxvalidation').style.display = "none";
	    	}
	    }

	    if(($('#editaudioUrl').val() != '' || $('#edit-audio-file-selector').val() != '') && ($('#editaudioqsttitle').val() != '') && editaudioinputfilled >= 2 && editaudioinputfilled > 0 && editaudiocheckedinput <=2 && editaudiocheckedinput>0) {
	    	document.getElementById("editaudioqstProgress").style.display = "block";
	    }

	    editaudioinputfilled = 0;
	    editaudiocheckedinput = 0;
    });

    /* Files Element Validations */

    $("#saveimgelementQuestion").on('click', function(event) {
        event.preventDefault();

        if($('#imageelementUrl').val() == '' && $('#imgelement').val() == '') {
        	document.getElementById('imgelementfilealert').style.display = "block";
        } else {
        	document.getElementById('imgelementfilealert').style.display = "none";
        }

        if($('#imgelementtitle').val() == '') {
        	document.getElementById('imgelementtitlealert').style.display = "block";
    	} else {
    		document.getElementById('imgelementtitlealert').style.display = "none";
    	}

    	if(($('#imageelementUrl').val() != '' || $('#imgelement').val() != '') && ($('#imgelementtitle').val() != '')) {
	    	document.getElementById("imgElementProgress").style.display = "block";
	    }
    });

    $("#editimgelt").on('click', function(event) {
        event.preventDefault();

        /*if($('#editimageelementUrl').val() == '' && $('#editimgelementquestion').val() == '') {
        	document.getElementById('editimgelementfilealert').style.display = "block";
        } else {
        	document.getElementById('editimgelementfilealert').style.display = "none";
        }*/

        if($('#editimgelementtitle').val() == '') {
        	document.getElementById('editimgelementtitlealert').style.display = "block";
    	} else {
    		document.getElementById('editimgelementtitlealert').style.display = "none";
    	}

    	if(($('#editimageelementUrl').val() != '' || $('#editimgelementquestion').val() != '') && ($('#editimgelementtitle').val() != '')) {
	    	document.getElementById("editimgElementProgress").style.display = "block";
	    } else {
	    	document.getElementById("editimgElementProgress").style.display = "none";
	    }
    });

    $("#savevideoelementQuestion").on('click', function(event) {
        event.preventDefault();

        if($('#videoelementUrl').val() == '' && $('#videoelementfile').val() == '') {
        	document.getElementById('videoelementfilealert').style.display = "block";
        } else {
        	document.getElementById('videoelementfilealert').style.display = "none";
        }

        if($('#videoelementtitle').val() == '') {
        	document.getElementById('videoelementtitlealert').style.display = "block";
    	} else {
    		document.getElementById('videoelementtitlealert').style.display = "none";
    	}

    	if(($('#videoelementUrl').val() != '' || $('#videoelementfile').val() != '') && ($('#videoelementtitle').val() != '')) {
	    	document.getElementById("videoElementProgress").style.display = "block";
	    }
    });

    $("#editvideoelt").on('click', function(event) {
        event.preventDefault();

        /*if($('#editvideoelementUrl').val() == '' && $('#editvideoelementquestion').val() == '') {
        	document.getElementById('editvideoelementfilealert').style.display = "block";
        } else {
        	document.getElementById('editvideoelementfilealert').style.display = "none";
        }*/

        if($('#editvideoelttitle').val() == '') {
        	document.getElementById('editvideoelementtitlealert').style.display = "block";
    	} else {
    		document.getElementById('editvideoelementtitlealert').style.display = "none";
    	}

    	if(($('#editvideoelementUrl').val() != '' || $('#editvideoelementquestion').val() != '') && ($('#editvideoelttitle').val() != '')) {
	    	document.getElementById("editvideoElementProgress").style.display = "block";
	    } else {
	    	document.getElementById("editvideoElementProgress").style.display = "none";
	    }
    });

    $("#saveaudioelementQuestion").on('click', function(event) {
        event.preventDefault();

        if($('#audioelementUrl').val() == '' && $('#audio-fileelement-selector').val() == '') {
        	document.getElementById('audioelementfilealert').style.display = "block";
        } else {
        	document.getElementById('audioelementfilealert').style.display = "none";
        }

        if($('#audioelementtitle').val() == '') {
        	document.getElementById('audioelementtitlealert').style.display = "block";
    	} else {
    		document.getElementById('audioelementtitlealert').style.display = "none";
    	}

    	if(($('#audioelementUrl').val() != '' || $('#audio-fileelement-selector').val() != '') && ($('#audioelementtitle').val() != '')) {
	    	document.getElementById("audioElementProgress").style.display = "block";
	    }
    });

    $("#editaudioelt").on('click', function(event) {
        event.preventDefault();

        /*if($('#editaudioelementUrl').val() == '' && $('#edit-audioelement-file-selector').val() == '') {
        	document.getElementById('editaudioelementfilealert').style.display = "block";
        } else {
        	document.getElementById('editaudioelementfilealert').style.display = "none";
        }*/

        if($('#editaudioelttitle').val() == '') {
        	document.getElementById('editaudioelementtitlealert').style.display = "block";
    	} else {
    		document.getElementById('editaudioelementtitlealert').style.display = "none";
    	}

    	if(($('#editaudioelementUrl').val() != '' || $('#edit-audioelement-file-selector').val() != '') && ($('#editaudioelttitle').val() != '')) {
	    	document.getElementById("editaudioElementProgress").style.display = "block";
	    } else {
	    	document.getElementById("editaudioElementProgress").style.display = "none";
	    }
    });

    $("#closemodalaudioelement").on('click', function(event) {
    	event.preventDefault();

    	document.getElementById("edit_output_audioelement").pause();
    });

    $("#closemodalvideoelement").on('click', function(event) {
    	event.preventDefault();

    	document.getElementById("edit_output_videoelement").pause();
    });

    $("#closemodalaudioqst").on('click', function(event) {
    	event.preventDefault();

    	document.getElementById("edit_output_audio").pause();
    });

    $("#closemodalvideoqst").on('click', function(event) {
    	event.preventDefault();

    	document.getElementById("edit_output_video").pause();
    });



    $("#closebtnaddaudioelt").on('click', function(event) {
    	event.preventDefault();

    	document.getElementById("output_audioelement").pause();
    });
    $("#closebtnaddvideoelt").on('click', function(event) {
    	event.preventDefault();

    	document.getElementById("output_videoelement").pause();
    });

    $("#closebtnaddaudioqst").on('click', function(event) {
    	event.preventDefault();

    	document.getElementById("output_audio").pause();
    });
    $("#closebtnaddvideoqst").on('click', function(event) {
    	event.preventDefault();

    	document.getElementById("output_video").pause();
    });
});

